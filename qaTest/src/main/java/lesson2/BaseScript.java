package lesson2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class BaseScript {

    WebDriver driver;

    public static WebDriver getDriver() {

        // Driver initialize
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");

        // Browser open
        return new ChromeDriver();

    }

}
