package lesson2.tests;

import lesson2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class loginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {

        //get webdriver instance
        WebDriver driver = getDriver();

        // Page Load
        pageLoad(driver);

        Thread.sleep(5000);

        // Initialize and click user pict
        WebElement userPict = driver.findElement(By.xpath("(//IMG[@class='imgm img-thumbnail'])[1]"));
        userPict.click();

        Thread.sleep(3000);

        // Initialize and click exit button
        WebElement exitButton = driver.findElement(By.xpath("//A[@id='header_logout']"));
        exitButton.click();

        Thread.sleep(5000);

        //Close the browser
        driver.quit();

    }

    public static void pageLoad(WebDriver driver) throws InterruptedException {
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

        Thread.sleep(3000);

        // Form Fields initialize and submit
        WebElement login = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.name("passwd"));
        login.sendKeys("webinar.test@gmail.com");
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        password.submit();
    }

}
