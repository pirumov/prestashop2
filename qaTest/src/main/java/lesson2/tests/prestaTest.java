package lesson2.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class prestaTest extends loginTest {

    public static void main(String[] args) throws InterruptedException {

        // get webdriver instance
        WebDriver driver = getDriver();

        // Page Load and authorization
        pageLoad(driver);

        Thread.sleep(5000);

        // get Number of elements for check
        List <WebElement> menuElements = driver.findElements(By.xpath("//ul[@class='menu' or @class='main-menu']/li/a"));

        System.out.println("Number of elements for check: " + menuElements.size());

        for (int i = 0; i < menuElements.size(); i++) {

            // refreshing elements connections
            List <WebElement> navElements = driver.findElements(By.xpath("//ul[@class='menu' or @class='main-menu']/li/a"));

            // click on menu elements
            navElements.get(i).click();
            Thread.sleep(5000);

            // save initial names
            String currentURL = driver.getCurrentUrl();
            String currentTitle = driver.getTitle();

            // print page title
            titlePrint(driver);
            System.out.println("Refreshing...");

            // page refreshing
            driver.navigate().refresh();
            Thread.sleep(5000);

            // get page's names after refreshing
            String refreshedURL = driver.getCurrentUrl();
            String refreshedTitle = driver.getTitle();

            // check for differents
            if (!currentURL.equals(refreshedURL)){
                System.out.println("Error: URLs are different!");
            } else if (!currentTitle.equals(refreshedTitle)){
                System.out.println("Error: Titles are different!");
            } else System.out.println("OK! The same pages!\n----------------------------------------------");


        }

        // Close the browser
        driver.quit();

    }

    private static void titlePrint(WebDriver driver) throws InterruptedException {
        System.out.println("Title is: " + driver.getTitle());
    }

}
